clear
echo [LOG] reseting directories...
rm -rf ./out
rm -rf ./api/pages
echo [LOG] creating folder structure...
mkdir ./out
mkdir ./out/data
mkdir ./api/pages
echo [LOG] building target...
cp -r ./public/* ./out/
cp -r ./src/* ./out/data/
rm -rf ./out/data/pages/
cp -r ./src/pages/* ./api/pages/
echo [LOG] done!