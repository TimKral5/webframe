(() => {
   const urlParams = new URLSearchParams(window.location.search);
   const page = urlParams.get('page') ?? "index";
   let path = new String(null);
   ((cb) => {
      const api = "https://api.timkral.com/website.php";
      const query = new URLSearchParams();

      query.set("page", page);
      const req = new XMLHttpRequest();
      req.open("GET", `${api}?${query.toString()}`);
      req.send();
      req.addEventListener("loadend", () => cb(req));
   })((/** @type {XMLHttpRequest} */ res) => {
      console.log(res.responseText);

      const xml = res.responseXML;
      xml.querySelectorAll();

   });
   console.log(path);
})();