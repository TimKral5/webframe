((func) => {
   const _ns = "ks";
   const ns = {};
   globalThis[_ns] = ns;
   func(ns, _ns);
})((ns, _ns) => {

   ns.handleLinks = (attr) =>
      document.querySelectorAll(`[${attr}]`).forEach(el => {

         el.addEventListener("click", () => {
            parent.location = el.getAttribute(attr).replace("{ns}", _ns);
         });

         el.addEventListener("mouseover", () => {
            if (!el.classList.contains("hover"))
               el.classList.add("hover");
         });

         el.addEventListener("mouseout", () => {
            if (el.classList.contains("hover"))
               el.classList.remove("hover");
         });

      });
});